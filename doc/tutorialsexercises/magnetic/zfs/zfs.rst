.. _zfs:

====================
Zero-field splitting
====================

.. warning:: **Work in progress**

.. module:: gpaw.zero_field_splitting
.. autofunction:: zfs
.. autofunction:: convert_tensor


Examples
========

Diamond NV- center
------------------

:download:`diamond_nv_minus.py`.


Biradical
---------

:download:`biradical.py`.
:download:`plot.py`.
